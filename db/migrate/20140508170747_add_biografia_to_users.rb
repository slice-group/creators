class AddBiografiaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :biografia, :text
  end
end
