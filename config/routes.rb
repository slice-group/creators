Creators::Application.routes.draw do
  
  resources :admin, only: [:index]
  resources :posts do
    member do
      put "like", to: "posts#upvote"
      put "dislike", to: "posts#downvote"
    end
  end

  match "/posts/add_new_comment" => "staticpage#add_new_comment", :as => "add_new_comment_to_posts", :via => [:post]
  get '/my_posts', to: 'staticpage#my_posts', as: 'my_posts'
  get '/my_posts/:id/preview', to: 'staticpage#preview', as: 'preview'
  get '/show/:id', to: 'staticpage#show', as: 'show_staticpage'
  get '/user/:id', to: 'admin#user', as: 'show_user'
  get 'admin/:id/publish_unpublish_post', to: 'admin#publish_unpublish_post', as: 'public'
  get 'admin/post/:id/comments',  to: 'admin#posts_comments', as: 'comments'
  delete '/comment/:id', :to => 'admin#destroy', as: 'delete_comments'
  get "/:category" => 'staticpage#home', as: :home_category
  get 'user/perfil/:id',  to: 'staticpage#perfil_usuario', as: 'perfil'
  post "staticpage/enviar_contacto", path: "enviarContacto"

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'staticpage#home'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
