class StaticpageController < ApplicationController
  def home
    if params[:search]
      @posts = Post.search(params[:search])
      if @posts.class == Array
        @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(6) 
      else
        @posts = @posts.page(params[:page]).per(6)
      end
    elsif params[:category]
      @posts = params[:category] == "0" ? Post.where("category_id != ? AND public = ?", "3", true).page(params[:page]) : Post.where("category_id = ? AND public = ?", params[:category], true).page(params[:page])
    else 
      @posts = Post.where("category_id != ? AND public = ?", "3", true).page(params[:page])
    end
  end

  def my_posts
    @posts = Post.where(user_id: current_user.id)
  end

  def preview
  	@post = Post.find(params[:id])
  end

  def show
  	@post = Post.find(params[:id])
  end

  def add_new_comment
    post = Post.find(params[:id])
    post.comments << Comment.new(comment: params[:comment], user_id: params[:user_id], name: params[:name])
    redirect_to show_staticpage_path(post.id, anchor: 'comment-area')
  end

  def posts_category
    @posts = params[:category] == "0" ? Post.where("category_id != ? AND public = ?", "4", true).page(params[:page]) : Post.where("category_id = ? AND public = ?", params[:category], true).page(params[:page])
  end

  def perfil_usuario
    @posts = Post.where(user_id: params[:id]).page(params[:page])
    @usuario = User.find(params[:id])
  end

  def enviar_contacto
    #if simple_captcha_valid?
      ContactMailer.contact(params).deliver
      redirect_to root_path, notice: params[:name]+', ¡Tu mensaje ha sido en enviado!'
    #else
      #redirect_to root_path(anchor: "contactanos"), alert: 'Error al ingresar el captcha, vuelva a intentar'
    #end
  end
end
