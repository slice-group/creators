class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #include SimpleCaptcha::ControllerHelpers
  protect_from_forgery with: :exception

  before_filter :update_sanitized_params, if: :devise_controller?

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:name, :cedula, :phone, :date_of_birth, :email, :password, :city, :password_confirmation)}
    devise_parameter_sanitizer.for(:account_update) do |u|
 			u.permit(:cedula, :phone, :date_of_birth, :avatar, :avatar_cache, :biografia, :current_password)
 		end
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = exception.message
    redirect_to new_user_session_path
  end
end
