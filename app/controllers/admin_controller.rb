class AdminController < ApplicationController
	authorize_resource :class => false #abiity cancan uso para solo controlador
	layout 'layouts/admin'
	before_filter :authenticate_user!

  def posts_comments
    @post = Post.find(params[:id])
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to comments_path(@comment.commentable_id) }
    end
  end

  def index
  	@category = Category.find(current_user.role)
  	@posts = Post.where(category_id: current_user.role)
  end

  def publish_unpublish_post
  	@post = Post.find(params[:id])
  	if @post.update_attribute(:public, @post.public == true ? false : true)
  		redirect_to admin_index_path
  	end

    #autorizar publicaciones de post al administrador que cumpla con el role
    if @post.category_id != current_user.role.to_i
      raise CanCan::AccessDenied.new("No estas autorizado para publicar este post.", :publish_unpublish_post, Post)
    end 
  end

  def user
    @user = User.find(params[:id])
  end
end
