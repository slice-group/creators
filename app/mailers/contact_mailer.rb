class ContactMailer < ActionMailer::Base
  default from: "info@jovenescreadores.org"

  def contact(contacto)
    @name = contacto[:name]
    @email = contacto[:email]
    @comment = contacto[:comment]
    mail(to: "info@jovenescreadores.org", subject: @name+' ha contactado con Mirasol.')
  end
end
