function onepage() {

	$('.top-nav').onePageNav({
    currentClass: 'active',
    changeHash: false,
    scrollSpeed: 500,
    filter: ':not(.external)',
    begin: function() {

    },
    end: function() {
      if ($('.top-nav .active').find('a').attr('href') == "/#home") {
        $(".scroll-up").animate({opacity: '0'}, 500 );
      } else {
        $(".scroll-up").animate({opacity: '1'}, 500 );
      }

    },
    scrollChange: function($currentListItem) {
      if ($('.top-nav .active').find('a').attr('href') == "/#home") {
        $(".scroll-up").animate({opacity: '0'}, 500 );
      } else {
        $(".scroll-up").animate({opacity: '1'}, 500 );
      }
		  
    }
  });

}

jQuery.extend(jQuery.validator.messages, {
  required: "Este campo es obligatorio.",
  remote: "Por favor, rellena este campo.",
  email: "Por favor, escribe una dirección de correo válida",
  url: "Por favor, escribe una URL válida.",
  date: "Por favor, escribe una fecha válida.",
  dateISO: "Por favor, escribe una fecha (ISO) válida.",
  number: "Por favor, escribe un número entero válido.",
  digits: "Por favor, escribe sólo dígitos.",
  creditcard: "Por favor, escribe un número de tarjeta válido.",
  equalTo: "Por favor, escribe el mismo valor de nuevo.",
  accept: "Por favor, escribe un valor con una extensión aceptada.",
  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
});

$(function(){ //short for $(document).ready(function(){
  $('.contact_form').validate({
    rules: {
      "email": {
        required: true,
        email: true,
        maxlength: 40
      },
      "name": {
        required: true,
        maxlength: 30
      },
      "comment": {
        required: true,
        maxlength: 1000,
        minlength: 40
      }
    }
  });
});