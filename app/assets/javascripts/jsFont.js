function VerticalCarousel(list, duration) {
    this.cont = list
    this.height = this.cont.parent().height();
    // Check if there is enough content
    if(this.cont.height() > this.height) {
        this.timer = null;
        this.duration = duration;
        this.index = 0;
        // Clone items for infinite scrolling
        this.cont.children().clone().appendTo(this.cont);
        // Update Item List and Count after clone
        this.items = this.cont.children();
        this.itemCount = this.items.length;
        // Store Item Height in an array
        this.itemHeights = [];
        for(var i = 0; i < this.itemCount; i++) {
            this.itemHeights.push(this.items.eq(i).outerHeight());
        }
        var self = this;
        self.hidePartial();
        self.startTimer();
    }
}

VerticalCarousel.prototype.slide = function() {
    var self = this;
    self.cont.animate({'marginTop': '-=' + (self.itemHeights[self.index])}, 500, function() {
        if(self.index == self.itemCount / 2) {
            self.cont.css('marginTop', 0);
            self.index = 0;
            self.hidePartial();
        }
        self.startTimer();
    });
    self.index++;
    self.hidePartial();
};

VerticalCarousel.prototype.hidePartial = function() {
    var self = this;
    var totalHeight = 0;
    for(var i = 0; i < self.itemCount * 2; i++) {
        self.items.eq(i).removeClass('visible-carousel-item');
        if(i < self.index) { 
            self.items.eq(i).addClass('visible-carousel-item');
        } else {
            totalHeight += self.itemHeights[i];
            if(totalHeight < self.height) {
                self.items.eq(i).addClass('visible-carousel-item');
            }
        } 
    }
};

VerticalCarousel.prototype.startTimer = function() {
    var self = this;
    self.timer = setTimeout(function() {
        self.slide();
    }, self.duration);
};

VerticalCarousel.prototype.clearTimer = function() {
    var self = this;
    clearTimeout(self.timer);
};

$(document).ready(function() {
    var vCar = new VerticalCarousel($('.v-carousel-mask ul'), 5000);
});

