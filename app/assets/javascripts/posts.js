function category_inputs() {
	$("#post_category_id").change(function(){
		if (this.value == 3) {
			$("#content").css("display", "none");
			$("#btn-select").css("display", "block");
		} 
		else {
			$("#content").css("display", "block");
			$("#btn-select").css("display", "none");
			$("#post_video").val("");
		}
	});
}

function input_show(type) {
	if(type=='images') {
		$("#images").css("display", "block");
		$("#video").css("display", "none");
	} else if (type=='video') {
		$("#video").css("display", "block");
		$("#images").css("display", "none");
	}
}