class Post < ActiveRecord::Base
  has_many :attachments
  accepts_nested_attributes_for :attachments

	before_create :default_values
	before_save :validation_values, :add_city
	acts_as_commentable
  acts_as_votable
  paginates_per 6

  validates_presence_of :title, :user_id, :category_id
  validates :title, uniqueness: true
  validate :validation_audiovisual?, :validation_content?

  #Posts narrativas
  def self.narrativas
    Post.where(category_id: 1)
  end

  #Posts poesias
  def self.poesias
    Post.where(category_id: 2)
  end

  #Posts audiovisuales
  def self.audiovisuales
    Post.where(category_id: 3)
  end

  #search
  def self.search(search)
    if search
      @post = find(:all, :conditions => ['autor iLIKE ? OR title iLIKE ? OR city iLIKE ?', ["%#{search}%"]*3].flatten)
    else
      @post = find(:all)
    end
  end
  #--------------------------------

	def user
		User.find(self.user_id)
	end

  
  def category
    Category.find(self.category_id).name
  end

  def video_conversion
    self.video.gsub('watch?v=', 'embed/')
  end

  #---------------------------------

	private

  def default_values
    self.public = 0
  end

  def add_city
    self.city = User.find(self.user_id).city
  end

  def validation_values
  	unless self.category_id == 3
    	self.video = "N/A"
    else
    	self.content = "N/A"
    end
      self.autor = User.find(self.user_id).name
  end

  def validation_audiovisual?
    if self.category_id == 3 and self.attachments.blank? and self.video.blank?
      errors.add(:audiovisual, "debe al menos subir un video o una imagen")
    end 
  end

  def validation_content?
    if self.category_id != 3 and self.content.blank?
      errors.add(:contenido, "no puede estar en blanco")
    end 
  end
end
