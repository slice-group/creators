class Ability
  include CanCan::Ability
  def initialize(user)
    user ||= User.new # guest user
   # raise user.role?(:administrator).inspect
    if user.admin?
      can :manage, Post
      can [:index, :publish_unpublish_post, :user, :posts_comments, :destroy], :admin
    else
      can [:create, :upvote, :downvote], Post
    end

  end
end
