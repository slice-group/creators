class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :confirmable
  mount_uploader :avatar, AvatarUploader

  devise :registerable if User.count < 205 #permitir registrarse hasta que la cantidad de usuarios sea de 205

  validates_numericality_of :cedula

  validates_numericality_of :phone

  validates_presence_of :name, :cedula, :phone, :date_of_birth

  validates :biografia, :length => { :maximum => 200 }

  validate :avatar_size_validation, :if => "avatar?"
   
  validates :biografia, :presence => {:on => :update}
   
  def admin?
    "normal" == self.role ? false : true
  end

  def comments_count
    Comment.count(conditions: "user_id = #{self.id}")
  end

  #metodo que verifica si hay permisos para registrarse
  def self.permit_register?
    User.count < 205
  end

  def permit_post?
    Comment.where(user_id: self.id).select(:commentable_id).uniq.count >=3 ? true : false
  end

  def cantidad_de_comentarios
    Comment.where(user_id: self.id).select(:commentable_id).uniq.count
  end

  def cantidad_de_posts
    Post.where(user_id: self.id).count
  end

  def data_blank?
    self.avatar.blank? or self.biografia.blank? or self.phone.blank? or self.date_of_birth.blank? ? true : false
  end

  def avatar_size_validation
    errors[:avatar] << "no puede ser mayor a 1MB" if avatar.size > 1.megabytes
  end
end
