# kaminari-i18n

## I18n support for Kaminari

This is a Rails gem that brings internationalization to kaminari in a super simple way, it's just a generator that copies the following template for each locale you want to install:

``` rb
#{locale_name}:
  views:
    pagination:
      first: "&laquo;"
      last: "&raquo;"
      previous: "&lsaquo;"
      next: "&rsaquo;"
      truncate: "..."
   
```

### Install

``` rb
gem 'kaminari-i18n'
```

### Usage

```
rails g kaminari:locale en
```

== Contributing to kaminari-i18n
 
* Check out the latest master to make sure the feature hasn't been implemented or the bug hasn't been fixed yet.
* Check out the issue tracker to make sure someone already hasn't requested it and/or contributed it.
* Fork the project.
* Start a feature/bugfix branch.
* Commit and push until you are happy with your contribution.
* Make sure to add tests for it. This is important so I don't break it in a future version unintentionally.
* Please try not to mess with the Rakefile, version, or history. If you want to have your own version, or is otherwise necessary, that is fine, but please isolate to its own commit so I can cherry-pick around it.

== Copyright

Copyright (c) 2012 mcasimir. See LICENSE.txt for
further details.

