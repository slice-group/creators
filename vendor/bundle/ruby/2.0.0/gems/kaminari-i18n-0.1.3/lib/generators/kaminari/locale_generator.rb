module Kaminari
  class LocaleGenerator < Rails::Generators::NamedBase
    
    def install
      create_file "config/locales/kaminari.#{name}.yml" do
<<-eos
#{name}:
  views:
    pagination:
      first: "&laquo;"
      last: "&raquo;"
      previous: "&lsaquo;"
      next: "&rsaquo;"
      truncate: "..."
        
eos
        
      end
    end
    
  end
end