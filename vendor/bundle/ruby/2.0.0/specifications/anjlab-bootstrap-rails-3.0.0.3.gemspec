# -*- encoding: utf-8 -*-
# stub: anjlab-bootstrap-rails 3.0.0.3 ruby lib

Gem::Specification.new do |s|
  s.name = "anjlab-bootstrap-rails"
  s.version = "3.0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Yury Korolev"]
  s.date = "2013-09-19"
  s.description = "Twitter Bootstrap CSS (with Sass flavour) and JS toolkits for Rails 3 projects"
  s.email = ["yury.korolev@gmail.com"]
  s.homepage = "https://github.com/anjlab/bootstrap-rails"
  s.rubygems_version = "2.2.2"
  s.summary = "Bootstrap CSS (with Sass flavour) and JS toolkits for Rails 3 projects"

  s.installed_by_version = "2.2.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<railties>, [">= 3.0"])
      s.add_runtime_dependency(%q<sass>, [">= 3.2"])
      s.add_development_dependency(%q<bundler>, [">= 1.0"])
      s.add_development_dependency(%q<rails>, [">= 3.1"])
    else
      s.add_dependency(%q<railties>, [">= 3.0"])
      s.add_dependency(%q<sass>, [">= 3.2"])
      s.add_dependency(%q<bundler>, [">= 1.0"])
      s.add_dependency(%q<rails>, [">= 3.1"])
    end
  else
    s.add_dependency(%q<railties>, [">= 3.0"])
    s.add_dependency(%q<sass>, [">= 3.2"])
    s.add_dependency(%q<bundler>, [">= 1.0"])
    s.add_dependency(%q<rails>, [">= 3.1"])
  end
end
