# -*- encoding: utf-8 -*-
# stub: glebtv-simple_captcha 0.6.2 ruby lib

Gem::Specification.new do |s|
  s.name = "glebtv-simple_captcha"
  s.version = "0.6.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["GlebTv"]
  s.date = "2014-04-02"
  s.description = "."
  s.email = "glebtv@gmail.com"
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc"]
  s.homepage = "http://github.com/glebtv/simple-captcha"
  s.rubygems_version = "2.2.2"
  s.summary = "A fork of a fork of a fork of simple_captcha."

  s.installed_by_version = "2.2.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<activesupport>, [">= 3.2"])
    else
      s.add_dependency(%q<activesupport>, [">= 3.2"])
    end
  else
    s.add_dependency(%q<activesupport>, [">= 3.2"])
  end
end
